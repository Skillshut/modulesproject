package ru.neoflex.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.neoflex.model.Topic;

public interface TopicRepository extends JpaRepository<Topic, Integer> {

    Topic findTopicById(Integer id);
}
