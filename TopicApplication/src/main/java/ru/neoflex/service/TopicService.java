package ru.neoflex.service;


import ru.neoflex.model.Topic;

import java.util.List;

public interface TopicService {

    Topic findTopicById(Integer id);

    Topic addTopic(Topic topic);

    Topic updateTopic(Topic topic, Integer id);

    void deleteTopic(Integer id);

    List<Topic> getAllTopics();
}
