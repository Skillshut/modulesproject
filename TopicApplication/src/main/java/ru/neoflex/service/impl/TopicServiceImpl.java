package ru.neoflex.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.neoflex.model.Topic;
import ru.neoflex.repository.TopicRepository;
import ru.neoflex.service.TopicService;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {


    private final TopicRepository topicRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Override
    @Transactional
    public Topic findTopicById(Integer id) {
        return topicRepository.findTopicById(id);
    }

    @Override
    @Transactional
    public Topic addTopic(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    @Transactional
    public Topic updateTopic(Topic topic, Integer id) {
        topic.setId(id);
        return topicRepository.save(topic);
    }

    @Override
    @Transactional
    public void deleteTopic(Integer id) {
        topicRepository.deleteById(id);
    }

    @Override
    @Transactional
    public List<Topic> getAllTopics() {
        return topicRepository.findAll();
    }

}